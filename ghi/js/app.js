function createCard(name, description, pictureUrl,create,end,locationName) {
    return `
      <div class="card shadow-sm p-3 mb-5 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-body-secondary">${locationName}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-body-secondary">${create}-${end}</div>
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded',async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        // const data = await response.json();
        // const conference = data.conferences[0];
        // const nameTag = document.querySelector('.card-title');
        // nameTag.innerHTML = conference.name;




        // const detailUrl = `http://localhost:8000${conference.href}`;
        // const detailResponse = await fetch(detailUrl);
        // if (detailResponse.ok) {
        //   const details = await detailResponse.json();
        //   // add description to the HTML
        //   const descriptionTag = document.querySelector('.card-text');
        //   descriptionTag.innerHTML = details.conference.description;

        //   const imgTag = document.querySelector('.card-img-top');
        //   imgTag.src = details.conference.location.picture_url;

        // }
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const created = details.conference.created;
            const create = new Date(created)

            const ends = details.conference.ends;
            const end = new Date(ends)

            const locationName = details.conference.location.name
            const html = createCard(name, description, pictureUrl,create.toLocaleDateString(),end.toLocaleDateString(),locationName);
            const colTag = document.querySelector('.col');
            colTag.innerHTML += html;
            console.log(details)
          }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.error(e);
    }
});
