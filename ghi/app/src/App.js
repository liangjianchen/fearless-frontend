import logo from './logo.svg';
import './App.css';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import PresentationForm from './PresentationForm';
import ConferenceForm from './ConferenceForm';
import MainPage from './MainPage';
import {BrowserRouter, Route, Router, Routes} from "react-router-dom"

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className='container'>
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="locations">
              <Route path='new' element={<LocationForm />}></Route>
          </Route>
          <Route path="presentation">
              <Route path='new' element={<PresentationForm />}></Route>
          </Route>
          <Route path="conference">
              <Route path='new' element={<ConferenceForm />}></Route>
          </Route>

        </Routes>

        {/* <AttendeesList attendees={props.attendees} /> */}
      </div>
    </BrowserRouter>
  );
}

export default App;
