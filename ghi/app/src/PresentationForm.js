import React, { useEffect, useState } from 'react';

function PresentationForm () {
    const [conferences, setConferences] = useState([])

    const [formData, setFormData] = useState({
      presenter_name: '',
      presenter_email: '',
      company_name: '',
      title: '',
      synopsis: '',
      conference: '',
    })

    const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences);
        }
    }

    useEffect(() => {
        fetchData();
      }, []);

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
          ...formData,
          [inputName]: value
        });
    }

    const handleSubmit = async (event) => {
      event.preventDefault();
      const conferenceId = formData.conference;
      const locationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;

      const fetchConfig = {
        method: "post",
        //Because we are using one formData state object,
        //we can now pass it directly into our request!
        body: JSON.stringify(formData),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(locationUrl, fetchConfig);
      if (response.ok) {
        setFormData({
          presenter_name: '',
          presenter_email: '',
          company_name: '',
          title: '',
          synopsis: '',
          conference: '',
        });
      }
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input placeholder="Presenter name" onChange={handleFormChange} required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Presenter email" onChange={handleFormChange} required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Company name" onChange={handleFormChange} type="text" name="company_name" id="company_name" className="form-control"/>
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Title" required onChange={handleFormChange} type="text" name="title" id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea className="form-control" onChange={handleFormChange} id="synopsis" rows="3" name="synopsis" className="form-control"/>
              </div>
              <div className="mb-3">
                <select required name="conference" id="conference" onChange={handleFormChange} className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                  return (
                    <option key={conference.id} value={conference.id}>{conference.name}</option>
                  )
                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      );
}

export default PresentationForm;
